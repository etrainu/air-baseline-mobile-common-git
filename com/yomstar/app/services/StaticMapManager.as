package com.yomstar.app.services
{
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.events.ImageLoaderEvent;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.errors.IOError;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	import starling.textures.Texture;
	
	public class StaticMapManager extends EventDispatcher
	{
		public function StaticMapManager()
		{
		}
		public function getMapTile(geoData:GeoData):void{
			var mapLoader:Loader = Loader(Main.loaderPool.getObj());
			mapLoader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, onMapLoaded);
			mapLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onMapLoadError);
			var req:URLRequest = URLRequest(Main.urlRequestPool.getObj());
			req.requestHeaders = null;
			req.data = null;
			req.url= "http://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels/?mapSize="+geoData.tileWidth+","+geoData.tileHeight+"&zoomLevel="+geoData.zoom+"&pushpin="+geoData.latitude+","+geoData.longitude+"&format="+geoData.tileFromat+"&key=AiiU246TJT-STd8PI3UNOZUQUfmT5C1NRfzkum0gTLL6r6b86IJWZH0o6iKmQtd5"
			mapLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		protected function onMapLoaded(event:flash.events.Event):void
		{
			
			trace("onMapLoaded");
			var mapLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mapLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onMapLoaded);
			mapLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onMapLoadError);
			Main.loaderPool.returnObj(mapLoader);
			var loadedBitmap:Bitmap = event.currentTarget.loader.content as Bitmap;
			this.dispatchEvent(new ImageLoaderEvent(ImageLoaderEvent.IMAGE_RECIEVED, Texture.fromBitmap(loadedBitmap)));
			loadedBitmap.bitmapData.dispose();
		}
		protected function onMapLoadError(event:IOErrorEvent):void
		{
			
			trace("onMapLoadError");
			var mapLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mapLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onMapLoaded);
			mapLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onMapLoadError);
			Main.loaderPool.returnObj(mapLoader);
			this.dispatchEvent(event);
		}
	}
}