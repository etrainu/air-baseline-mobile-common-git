package com.yomstar.app.services
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.utils.Timer;
	
	public class APIService extends EventDispatcher
	{
		protected var isInProgress:Boolean;
		public function APIService()
		{
			isInProgress = false;
		}
		protected function setupDelayedCall():void{
			var timer:Timer = Timer(Main.timerPool.getObj());
			timer.delay = 1;
			timer.repeatCount = 1;
			timer.addEventListener(TimerEvent.TIMER, onDelayedCall);
			timer.start();
		}
		private function onDelayedCall(e:TimerEvent):void{
			var timer:Timer = e.target as Timer;
			timer.removeEventListener(TimerEvent.TIMER, onDelayedCall);
			timer.stop();
			Main.timerPool.returnObj(timer);
			delayedCall();
		}
		protected function delayedCall():void{
			
		}
		protected function onHttpStatus(e:HTTPStatusEvent):void{
			//trace("onHttpStatus: "+e.toString());
		}
		protected function onResult(e:Event):void{
			//trace("onResult"+e.toString());
			isInProgress = false;
		}
		protected function onIOError(e:IOErrorEvent):void{
			isInProgress = false;
			var apiCaller:URLLoader = e.currentTarget as URLLoader;
			removeListeners(apiCaller);
			if(String(apiCaller.data).length > 0){
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				//trace(e.toString());

				if(String(apiCaller.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(apiCaller.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(apiCaller);
		}
		protected function onServiceCallFailed(message:String):void{
			trace("message: "+message);
			this.dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, message));
		}
		protected function removeListeners(loaderObject:URLLoader):void{
			loaderObject.removeEventListener(Event.COMPLETE, onResult);
			loaderObject.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loaderObject.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
			loaderObject.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
		}
	}
}