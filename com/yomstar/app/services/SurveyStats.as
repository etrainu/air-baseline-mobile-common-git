package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.SurveyStat;
	import com.yomstar.app.utils.ObjectDigger;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class SurveyStats extends APIService
	{
		public static var surveyStats:SurveyStat;
		
		private var surveyStatsLoader:URLLoader;
		private var req:URLRequest;
		public function SurveyStats()
		{
			super();
		}
		public function getStats(token:String, surveyId:String):void{
			if(!isInProgress){
				isInProgress = true;
				surveyStatsLoader = URLLoader(Main.urlLoaderPool.getObj());
				req = URLRequest(Main.urlRequestPool.getObj());
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.BASELINE_STATISTICS_URL+surveyId;
				//trace("req.url"+req.url);
				//trace("key: "+token);
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				surveyStatsLoader.dataFormat = URLLoaderDataFormat.TEXT;
				surveyStatsLoader.addEventListener(Event.COMPLETE, onResult);
				surveyStatsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				surveyStatsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				surveyStatsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			surveyStatsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var surveyStatsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(surveyStatsLoader);
			if(String(surveyStatsLoader.data).length > 0){
				trace("surveyStatsLoader.data: "+surveyStatsLoader.data);
				trace("surveyStatsLoader.data: "+ObjectDigger.digg(JSON.parse(surveyStatsLoader.data)));
				surveyStats = new SurveyStat(JSON.parse(String(surveyStatsLoader.data)));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(surveyStatsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(surveyStatsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				Main.urlLoaderPool.returnObj(surveyStatsLoader);
			}
			
		}
	}
}