package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.RewardScheme;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.utils.ObjectDigger;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class RewardSchemes extends APIService
	{
		public static var rewardScheme:RewardScheme;
		
		private var rewardSchemeLoader:URLLoader
		private var req:URLRequest;
		private var _prevSchemeId:String;
		public function RewardSchemes()
		{
			super();
		}
		public function getRewardSchemeById(token:String, schemeId:String):void{
			if(!isInProgress){
				if(!rewardScheme && (_prevSchemeId!=schemeId)){
					rewardScheme = null;
					isInProgress = true;
					rewardSchemeLoader =  URLLoader(Main.urlLoaderPool.getObj());
					req = Main.urlRequestPool.getObj();
					req.requestHeaders = null;
					req.data = null;
					req.url = YomstarAPIGates.REWARD_SCHEME_FROM_CODE_URL+schemeId;
					req.requestHeaders = new Array(new URLRequestHeader("key", token));
					req.method = URLRequestMethod.GET;
					rewardSchemeLoader.dataFormat = URLLoaderDataFormat.TEXT;
					rewardSchemeLoader.addEventListener(Event.COMPLETE, onResult);
					rewardSchemeLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
					rewardSchemeLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
					rewardSchemeLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
					super.setupDelayedCall();
					_prevSchemeId = schemeId;
				}else{
					dispatchEvent(new Event(Event.COMPLETE));
					isInProgress = false;
				}
			}
				
			
		}
		override protected function delayedCall():void{
			rewardSchemeLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var rewardSchemeLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(rewardSchemeLoader);
			if(String(rewardSchemeLoader.data).length > 0 && String(rewardSchemeLoader.data).indexOf("error")==-1){
				trace("rewardSchemeLoader.data: "+rewardSchemeLoader.data);
				rewardScheme = new RewardScheme(JSON.parse(String(rewardSchemeLoader.data)));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(rewardSchemeLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(rewardSchemeLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(rewardSchemeLoader);
			
		}
	}
}