package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.SampleDetail;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class SampleDetails extends APIService
	{
		public static var sampleDetails:SampleDetail;
		
		private var sampleDetailsLoader:URLLoader
		private var req:URLRequest;
		public function SampleDetails()
		{
			super();
		}
		public function getDetails(token:String, sampleId:String):void{
			if(!isInProgress){
				isInProgress = true;
				sampleDetailsLoader =  URLLoader(Main.urlLoaderPool.getObj());
				req = Main.urlRequestPool.getObj();
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.SAMPLE_DETAILS_URL+sampleId;
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				sampleDetailsLoader.dataFormat = URLLoaderDataFormat.TEXT;
				sampleDetailsLoader.addEventListener(Event.COMPLETE, onResult);
				sampleDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				sampleDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				sampleDetailsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			sampleDetailsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var sampleDetailsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(sampleDetailsLoader);
			if(String(sampleDetailsLoader.data).length > 0){
				trace("sampleDetails: "+String(sampleDetailsLoader.data));
				sampleDetails = new SampleDetail(JSON.parse(String(sampleDetailsLoader.data)));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(sampleDetailsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(sampleDetailsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(sampleDetailsLoader);
			
		}
	}
}