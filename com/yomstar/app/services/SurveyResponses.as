package com.yomstar.app.services
{
	import com.yomstar.app.config.SurveySorting;
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.SampleData;
	import com.yomstar.app.events.AuthenticationEvent;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	public class SurveyResponses extends APIService
	{
		public static var surveyResponses:SampleData;
		public function SurveyResponses()
		{
			super();
		}
		public function getResponses(token:String, startIndex:uint, length:uint, sorting:String=SurveySorting.DESCENDING):void{
			var responsesLoader:URLLoader = URLLoader(Main.urlLoaderPool.getObj());
			var req:URLRequest = URLRequest(Main.urlRequestPool.getObj());
			req.requestHeaders = null;
			req.data = null;
			req.url = YomstarAPIGates.SURVEY_RESPONSE_URL+String(length)+"/"+String(startIndex);
			req.data = '{"sort":[{"sampleDateCreated":"'+sorting+'"}]}';
			req.contentType = "Content-Type:application/json";
			req.requestHeaders = new Array(new URLRequestHeader("key", token));
			req.method = URLRequestMethod.POST;
			responsesLoader.dataFormat = URLLoaderDataFormat.BINARY;
			responsesLoader.addEventListener(Event.COMPLETE, onResult);
			responsesLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			responsesLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
			responsesLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
			responsesLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		public function getResponsesForSurvey(token:String, startIndex:uint, length:uint, externalId:String, sorting:String=SurveySorting.DESCENDING):void{
			var responsesLoader:URLLoader = URLLoader(Main.urlLoaderPool.getObj());
			var req:URLRequest = URLRequest(Main.urlRequestPool.getObj());
			req.requestHeaders = null;
			req.data = null;
			req.url = YomstarAPIGates.SURVEY_RESPONSE_URL+String(length)+"/"+String(startIndex);
			req.contentType = "Content-Type:application/json";
			req.data = '{"sort":[{"sampleDateCreated":"'+sorting+'"}], "baseline.externalId":[{"is":"'+externalId+'"}]}';
			req.requestHeaders = new Array(new URLRequestHeader("key", token));
			trace("req.url"+req.url);
			trace("req.data"+req.data);
			trace("key: "+token);
			req.method = URLRequestMethod.POST;
			responsesLoader.dataFormat = URLLoaderDataFormat.TEXT;
			responsesLoader.addEventListener(Event.COMPLETE, onResult);
			responsesLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			responsesLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
			responsesLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
			responsesLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			var responsesLoader:URLLoader = e.currentTarget as URLLoader;
			trace("onLatestResponses");
			removeListeners(responsesLoader);
			if(String(responsesLoader.data).length > 0){
				trace("String(responsesLoader.data): "+String(responsesLoader.data));
				surveyResponses = new SampleData(JSON.parse(String(responsesLoader.data)));
				trace("surveyResponses: "+surveyResponses);
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(responsesLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(responsesLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(responsesLoader);
		}
	}
}