package com.yomstar.app.services
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	import com.yomstar.app.config.YomstarAPIGates;
	
	public class AccountDetails extends APIService
	{
		public static var accountDetails:Object;
		
		private var accountDetailsLoader:URLLoader
		private var req:URLRequest;
		public function AccountDetails()
		{
			super();
		}
		public function getDetails(token:String):void{
			if(!isInProgress){
				isInProgress = true;
				accountDetailsLoader =  URLLoader(Main.urlLoaderPool.getObj());
				req = Main.urlRequestPool.getObj();
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.ACCOUNT_DETAILS_URL;
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				accountDetailsLoader.dataFormat = URLLoaderDataFormat.TEXT;
				accountDetailsLoader.addEventListener(Event.COMPLETE, onResult);
				accountDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				accountDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				accountDetailsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			accountDetailsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var accountDetailsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(accountDetailsLoader);
			if(String(accountDetailsLoader.data).length > 0){
				accountDetails = JSON.parse(String(accountDetailsLoader.data));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(accountDetailsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(accountDetailsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(accountDetailsLoader);
			
		}
	}
}