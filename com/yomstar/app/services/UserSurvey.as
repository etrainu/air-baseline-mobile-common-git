
package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.BaselineData;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class UserSurvey extends APIService
	{
		public static var userSurveyInfo:BaselineData;
		
		private var userSurveyLoader:URLLoader;
		private var req:URLRequest;
		public function UserSurvey()
		{
			super();
		}
		public function getUserSurvey(token:String):void{
			if(!isInProgress){
				isInProgress = true;
				userSurveyLoader = URLLoader(Main.urlLoaderPool.getObj());
				req = URLRequest(Main.urlRequestPool.getObj());
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.USER_OWNED_SURVEY_URL;
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				userSurveyLoader.dataFormat = URLLoaderDataFormat.TEXT;
				userSurveyLoader.addEventListener(Event.COMPLETE, onResult);
				userSurveyLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				userSurveyLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				userSurveyLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			userSurveyLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var userSurveyLoader:URLLoader = e.currentTarget as URLLoader;
			userSurveyLoader.removeEventListener(Event.COMPLETE, onResult);
			userSurveyLoader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			if(String(userSurveyLoader.data).length > 0){
				userSurveyInfo = new BaselineData(JSON.parse(String(userSurveyLoader.data)));
				//trace("userSurveyInfo: "+userSurveyInfo);
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(userSurveyLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(userSurveyLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
			}
			Main.urlLoaderPool.returnObj(userSurveyLoader);
			
		}
	}
}

