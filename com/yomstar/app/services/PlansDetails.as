package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.utils.ObjectDigger;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class PlansDetails extends APIService
	{
		public static var plansDetails:Object;
		public static var activePlan:Object;
		
		private var plansDetailsLoader:URLLoader
		private var req:URLRequest;
		public function PlansDetails()
		{
			super();
		}
		public function getPlans():void{
			if(!isInProgress){
				isInProgress = true;
				if(!plansDetails){
					plansDetailsLoader =  URLLoader(Main.urlLoaderPool.getObj());
					req = URLRequest(Main.urlRequestPool.getObj());
					req.requestHeaders = null;
					req.data = null;
					req.url = YomstarAPIGates.PLANS_DETAILS_URL;
					plansDetailsLoader.dataFormat = URLLoaderDataFormat.TEXT;
					plansDetailsLoader.addEventListener(Event.COMPLETE, onResult);
					plansDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
					plansDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
					plansDetailsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
					plansDetailsLoader.load(req);
					Main.urlRequestPool.returnObj(req);
				}else{
					dispatchEvent(new Event(Event.COMPLETE));
				}
			}
			
		}
		public static function getPlanById(planId:String = "", setAsActivePlan:Boolean = true):Object{
			if(plansDetails){
				if(planId.length>0 || activePlan){
					var len:int = plansDetails.length;
					var indexFound:int = -1;
					for(var i:int=0; i<len; i++){
						if(String(planId) == String(plansDetails[i].planId)){
							indexFound = i;
							break
						}
					}
					if(indexFound >= 0){
						if(setAsActivePlan){
							activePlan = plansDetails[indexFound];
						}
						return plansDetails[indexFound]
					}else{
						throw new Error("Invalid plan ID");
					}
				}else{
					throw new Error("No active plan available. Plan ID must be set");
				}
				
			}else{
				throw new Error("No plan details received yet");
			}
			
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var plansDetailsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(plansDetailsLoader);
			if(String(plansDetailsLoader.data).length > 0){
				trace("plansDetailsLoader.data: "+plansDetailsLoader.data);
				plansDetails = JSON.parse(String(plansDetailsLoader.data));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(plansDetailsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(plansDetailsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(plansDetailsLoader);
		}
	}
}