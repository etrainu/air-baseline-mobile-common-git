package com.yomstar.app.services
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	import com.yomstar.app.config.YomstarAPIGates;
	
	public class UserInfo extends APIService
	{
		public static var userInfo:Object;
		
		private var userInfoLoader:URLLoader;
		private var req:URLRequest;
		public function UserInfo()
		{
			super();
		}
		public function getInfo(token:String):void{
			if(!isInProgress){
				if(!userInfo){
					isInProgress = true;
					userInfoLoader = URLLoader(Main.urlLoaderPool.getObj());
					req = URLRequest(Main.urlRequestPool.getObj());
					req.requestHeaders = null;
					req.data = null;
					req.url = YomstarAPIGates.USER_INFO_SEREVER_URL;
					req.requestHeaders = new Array(new URLRequestHeader("key", token));
					req.method = URLRequestMethod.GET;
					userInfoLoader.dataFormat = URLLoaderDataFormat.TEXT;
					userInfoLoader.addEventListener(Event.COMPLETE, onResult);
					userInfoLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
					userInfoLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
					userInfoLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
					super.setupDelayedCall();
				}else{
					dispatchEvent(new Event(Event.COMPLETE));
				}
				
			}
			
		}
		override protected function delayedCall():void{
			userInfoLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var userInfoLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(userInfoLoader);
			if(String(userInfoLoader.data).length > 0 && String(userInfoLoader.data).indexOf("error")==-1){
				userInfo = JSON.parse(String(userInfoLoader.data));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(userInfoLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(userInfoLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(userInfoLoader);
			
		}
	}
}