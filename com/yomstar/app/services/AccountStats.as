package com.yomstar.app.services
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	import com.yomstar.app.config.YomstarAPIGates;
	
	public class AccountStats extends APIService
	{
		public static var accountStats:Object;
		
		private var accountStatsLoader:URLLoader
		private var req:URLRequest;
		public function AccountStats()
		{
			super();
		}
		public function getStats(token:String):void{
			if(!isInProgress){
				isInProgress = true;
				accountStatsLoader =  URLLoader(Main.urlLoaderPool.getObj());
				req = Main.urlRequestPool.getObj();
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.ACCOUNT_STATISTICS_URL;
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				accountStatsLoader.dataFormat = URLLoaderDataFormat.TEXT;
				accountStatsLoader.addEventListener(Event.COMPLETE, onResult);
				accountStatsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				accountStatsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				accountStatsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			accountStatsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var accountStatsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(accountStatsLoader);
			if(String(accountStatsLoader.data).length > 0){
				accountStats = JSON.parse(String(accountStatsLoader.data));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(accountStatsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(accountStatsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(accountStatsLoader);
			
		}
	}
}