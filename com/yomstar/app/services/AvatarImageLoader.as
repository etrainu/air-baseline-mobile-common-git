package com.yomstar.app.services
{
	// import the MD5 hashing code:
	import com.adobe.crypto.MD5;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.utils.ScaleUtils;
	import com.yomstar.app.utils.ValidationCheck;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	import starling.textures.Texture;

	public class AvatarImageLoader extends EventDispatcher
	{
		private var _validRatings:Array = new Array('g', 'pg', 'r', 'x');
		private var _validDefaultImageTypes:Array = new Array('default', 'identicon', 'monsterid', 'wavatar');
		private var _email:String = new String();
		private var _size:Number;		// default size of 80 pixels
		private var _rating:String = "G";	// default rating of G
		private var _defaultImage:String;  // default to the blue 'G'
		
		public function AvatarImageLoader()
		{
			super();
		}
		public function getAvatar(email:String, imageSize:int = -1):void{
			if(imageSize<0){
				this.size = GlobalAppValues.USER_AVATAR_SIZE;
			}else{
				this.size = imageSize;
			}
			if(!_defaultImage){
				//_defaultImage = "default";
				_defaultImage = GlobalAppValues.DEFAULT_USER_AVATAR;
			}
			if(ValidationCheck.validateEmail(email)){
				this._email = email;
			}else{
				throw new Error("Provided email is not in a valid email format");
			}
			
			var mapLoader:Loader = Loader(Main.loaderPool.getObj());
			mapLoader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, onImageLoaded);
			mapLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onImageLoadError);
			var req:URLRequest = URLRequest(Main.urlRequestPool.getObj());
			req.requestHeaders = null;
			req.data = null;
			req.url= "http://www.gravatar.com/avatar/" +
				MD5.hash(_email).toString() + "?" +
				"size=" + _size +
				"&rating=" + _rating +
				"&d=" + _defaultImage;
			trace("req.url: "+req.url);
			mapLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		protected function onImageLoaded(event:flash.events.Event):void
		{
			trace("onImageLoaded");
			var mapLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mapLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onImageLoaded);
			mapLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onImageLoadError);
			Main.loaderPool.returnObj(mapLoader);
			var loadedBitmap:Bitmap = event.currentTarget.loader.content as Bitmap;
			var scale:Number = Math.min(1, _size/loadedBitmap.width);
			trace("scale: "+scale);
			trace("loadedBitmap: "+loadedBitmap);
			this.dispatchEvent(new ImageLoaderEvent(ImageLoaderEvent.IMAGE_RECIEVED, Texture.fromBitmap(loadedBitmap, true, true, 1/scale)));
			
			loadedBitmap.bitmapData.dispose();
		}
		protected function onImageLoadError(event:IOErrorEvent):void
		{
			trace("onImageLoadError");
			var mapLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mapLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onImageLoaded);
			mapLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onImageLoadError);
			Main.loaderPool.returnObj(mapLoader);
			this.dispatchEvent(event);
		}
		public function set email(value:String):void
		{
			// if the email is invalid, a default image will be returned:
			_email = value;
		}
		public function set size(value:Number):void
		{
			// sanity check on incoming value, must be between 1 and 512:
			if( value >= 1 && value < 512)
				_size = value;
			else
				_size = 80;
		}
		public function get size():Number
		{
			return _size
		}
		public function set rating(value:String):void
		{
			// do a sanity check on the rating, allowing values
			// only in the _validRatings array (defined above):
			if( _validRatings.indexOf( value.toLowerCase()) != -1)
				_rating = value;
		}
		public function set defaultImage(value:String):void
		{
			_defaultImage = value;
		}
	}
}