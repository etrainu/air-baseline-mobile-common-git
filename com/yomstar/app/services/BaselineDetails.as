
package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class BaselineDetails extends APIService
	{
		public static var baselineInfo:Object;
		
		private var baselineSurveyLoader:URLLoader;
		private var req:URLRequest;
		public function BaselineDetails()
		{
			super();
		}
		public function getSurveyById(token:String, baselineId:String):void{
			if(!isInProgress){
				isInProgress = true;
				baselineSurveyLoader = URLLoader(Main.urlLoaderPool.getObj());
				
				req = URLRequest(Main.urlRequestPool.getObj());
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.SURVEY_BY_BASELINE_ID_URL+baselineId;
				req.requestHeaders = new Array(new URLRequestHeader("key", token));
				req.method = URLRequestMethod.GET;
				baselineSurveyLoader.dataFormat = URLLoaderDataFormat.TEXT;
				baselineSurveyLoader.addEventListener(Event.COMPLETE, onResult);
				baselineSurveyLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				baselineSurveyLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				baselineSurveyLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				super.setupDelayedCall();
			}
			
		}
		override protected function delayedCall():void{
			baselineSurveyLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var baselineSurveyLoader:URLLoader = e.currentTarget as URLLoader;
			baselineSurveyLoader.removeEventListener(Event.COMPLETE, onResult);
			baselineSurveyLoader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			if(String(baselineSurveyLoader.data).length > 0 && String(baselineSurveyLoader.data).indexOf("error")==-1){
				trace("String(baselineSurveyLoader.data): "+String(baselineSurveyLoader.data));
				baselineInfo = JSON.parse(String(baselineSurveyLoader.data));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(baselineSurveyLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(baselineSurveyLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
			}
			Main.urlLoaderPool.returnObj(baselineSurveyLoader);
			
		}
	}
}

