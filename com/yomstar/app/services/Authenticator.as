package com.yomstar.app.services
{
	import com.yomstar.app.events.AuthenticationEvent;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import com.yomstar.app.config.YomstarAPIGates;
	
	public class Authenticator extends APIService
	{
		
		
		public function Authenticator()
		{
			super();
		}
		public function checkLogin(user:String, pass:String):void{
			if(!isInProgress){
				isInProgress = true;
				var authLoader:URLLoader = URLLoader(Main.urlLoaderPool.getObj());
				var req:URLRequest = Main.urlRequestPool.getObj();
				req.requestHeaders = null;
				req.data = null;
				req.url = YomstarAPIGates.AUTHENTICATOR_SEREVER_URL;
				var requestVariables:URLVariables = new URLVariables();
				requestVariables.password = pass;
				requestVariables.email = user;
				req.method = URLRequestMethod.GET;
				req.data = requestVariables;
				authLoader.dataFormat = URLLoaderDataFormat.TEXT;
				authLoader.addEventListener(Event.COMPLETE, onResult);
				authLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				authLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
				authLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				authLoader.load(req);
				
				Main.urlRequestPool.returnObj(req);
			}
			
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var authLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(authLoader);
			if(String(authLoader.data).length > 0 && String(authLoader.data).indexOf("error")==-1){
					var responseData:Object = JSON.parse(String(authLoader.data));
					if(responseData.token){
						if(String(responseData.token).length > 0){
							var evt:AuthenticationEvent = new AuthenticationEvent(AuthenticationEvent.LOGIN_SUCCESS, String(responseData.token), true, "");
							this.dispatchEvent(evt);
						}else{
							onServiceCallFailed("No active token received");
						}
					}else{
						onServiceCallFailed("No active token received");
					}
			}else if(String(authLoader.data).indexOf("error")>-1){
				var errorData:XML;
				errorData = XML(authLoader.data);
				onServiceCallFailed(errorData.message);
				
			}else{
				onServiceCallFailed("Unexpected server response:\n"+errorData.message);
			}
			Main.urlLoaderPool.returnObj(authLoader);
		}
		override protected function onServiceCallFailed(message:String):void{
			var evt:AuthenticationEvent = new AuthenticationEvent(AuthenticationEvent.LOGIN_FAILURE, "", false, message);
			this.dispatchEvent(evt);
		}
	}
}