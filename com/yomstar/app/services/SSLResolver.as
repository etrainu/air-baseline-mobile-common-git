package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.ui.mobile.utils.OSUtils;
	
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.system.Capabilities;

	public class SSLResolver extends EventDispatcher
	{
		import flash.media.StageWebView;
		
		public function SSLResolver()
		{
		}
		public function resolve(stage:Stage):void{
			if(OSUtils.isIOS()){
				var webView:StageWebView;
				webView = new  StageWebView();
				webView.stage = stage;
				webView.addEventListener(Event.COMPLETE, onComplete);
				webView.loadURL(YomstarAPIGates.AUTHENTICATOR_SEREVER_URL);
			}
		}
		private function onComplete(e:Event):void{
			trace("ssl resolved");
			var webView:StageWebView = e.currentTarget as StageWebView;
			webView.dispose();
		}
	}
}