package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.Reward;
	import com.yomstar.app.dataObjects.SampleDetail;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class RewardDetails extends APIService
	{
		public static var rewardDetails:Reward;
		
		private var rewardDetailsLoader:URLLoader
		private var req:URLRequest;
		private var _prevRewardId:String;
		public function RewardDetails()
		{
			super();
		}
		public function getRewardDetailsById(token:String, rewardId:String):void{
			if(!isInProgress){
				if(!rewardDetails && (_prevRewardId!=rewardId)){
					isInProgress = true;
					rewardDetails = null; 
					rewardDetailsLoader =  URLLoader(Main.urlLoaderPool.getObj());
					req = Main.urlRequestPool.getObj();
					req.requestHeaders = null;
					req.data = null;
					req.url = YomstarAPIGates.REWARD_DETAILS_FROM_CODE_URL+rewardId;
					req.requestHeaders = new Array(new URLRequestHeader("key", token));
					req.method = URLRequestMethod.GET;
					rewardDetailsLoader.dataFormat = URLLoaderDataFormat.TEXT;
					rewardDetailsLoader.addEventListener(Event.COMPLETE, onResult);
					rewardDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
					rewardDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
					rewardDetailsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
					super.setupDelayedCall();
					_prevRewardId = rewardId;
				}else{
					dispatchEvent(new Event(Event.COMPLETE));
					isInProgress = false;
				}
			}
				
			
		}
		override protected function delayedCall():void{
			rewardDetailsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var rewardDetailsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(rewardDetailsLoader);
			if(String(rewardDetailsLoader.data).length > 0){
				trace("rewardDetailsLoader.data: "+rewardDetailsLoader.data);
				rewardDetails = new Reward(JSON.parse(String(rewardDetailsLoader.data)));
				dispatchEvent(new Event(Event.COMPLETE));
			}else{
				if(String(rewardDetailsLoader.data).indexOf("error")>-1){
					var errorData:XML;
					errorData = XML(rewardDetailsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					onServiceCallFailed("No server response");
				}
				
			}
			Main.urlLoaderPool.returnObj(rewardDetailsLoader);
			
		}
	}
}