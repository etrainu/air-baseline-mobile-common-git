package com.yomstar.app.services
{
	import com.yomstar.app.config.YomstarAPIGates;
	import com.yomstar.app.dataObjects.Participant;
	import com.yomstar.app.utils.ObjectDigger;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class ParticipantDetails extends APIService
	{
		public static var participantDetails:Participant;
		public static var activePlan:Object;
		
		private var participantDetailsLoader:URLLoader
		private var req:URLRequest;
		private var prevParticipantId:String;
		public function ParticipantDetails()
		{
			super();
		}
		public function getParticipant(token:String, participantId:String):void{
			if(!isInProgress){
				if(!participantDetails && (prevParticipantId!=participantId)){
					participantDetails = null;
					isInProgress = true;
					participantDetailsLoader =  URLLoader(Main.urlLoaderPool.getObj());
					req = Main.urlRequestPool.getObj();
					req.requestHeaders = null;
					req.data = null;
					req.url = YomstarAPIGates.PARTICIPANT_DETAILS_URL+participantId;
					req.requestHeaders = new Array(new URLRequestHeader("key", token));
					req.method = URLRequestMethod.GET;
					participantDetailsLoader.dataFormat = URLLoaderDataFormat.TEXT;
					participantDetailsLoader.addEventListener(Event.COMPLETE, onResult);
					participantDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHttpStatus);
					participantDetailsLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
					participantDetailsLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
					super.setupDelayedCall();
				}else{
					isInProgress = false;
					dispatchEvent(new Event(Event.COMPLETE));
				}
				
			}
			
		}
		override protected function delayedCall():void{
			participantDetailsLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		override protected function onResult(e:Event):void{
			isInProgress = false;
			var participantDetailsLoader:URLLoader = e.currentTarget as URLLoader;
			removeListeners(participantDetailsLoader);
			if(String(participantDetailsLoader.data).length > 0){
				if(String(participantDetailsLoader.data).toLowerCase().indexOf("error")!=-1){
					trace("participantDetailsLoader.data Error: "+participantDetailsLoader.data);
					var errorData:XML;
					errorData = XML(participantDetailsLoader.data);
					onServiceCallFailed(errorData.message);
					
				}else{
					//trace("participantDetailsLoader.data: "+participantDetailsLoader.data);
					participantDetails = new Participant(JSON.parse(String(participantDetailsLoader.data)));
					dispatchEvent(new Event(Event.COMPLETE));
				}
				
			}else{
				onServiceCallFailed("No server response");
			}
			Main.urlLoaderPool.returnObj(participantDetailsLoader);
			
		}
	}
}