package com.yomstar.app.config
{
	public class ResponseType
	{
		public static const FREE_TEXT_QUESTION_RESPONSE:String = "freetextanswer";
		public static const SLIDER_QUESTION_RESPONSE:String = "slideranswer";
		public static const MULTI_CHOICE_QUESTION_RESPONSE:String = "multichoiceanswer";
		public static const MEDIA_QUESTION_RESPONSE:String = "mediaanswer";
	}
}