package com.yomstar.app.config
{
	public class YomstarGlobalConsts
	{
		public static const MULTI_CHOICE_QUESTION_TYPE_TITLE:String = "MultiChoice Question";
		public static const SCALED_QUESTION_TYPE_TITLE:String = "Scaled Question";
		public static const FREE_TEXT_QUESTION_TYPE_TITLE:String = "Free Text Question";
		public static const MULTICHOICE_EXTRA_INFO_LABEL:String = "Other. Please specify:";
	}
}