package com.yomstar.app.config
{
	public class SurveySorting
	{
		public static const DESCENDING:String = "desc";
		public static const ASCENDING:String = "asc";
	}
}