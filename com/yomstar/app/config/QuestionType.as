package com.yomstar.app.config
{
	public class QuestionType
	{
		public static const FREE_TEXT_QUESTION:String = "freetextquestion";
		public static const SLIDER_QUESTION:String = "sliderquestion";
		public static const MULTI_CHOICE_QUESTION:String = "multichoicequestion";
		public static const MEDIA_QUESTION:String = "mediaquestion";
	}
}