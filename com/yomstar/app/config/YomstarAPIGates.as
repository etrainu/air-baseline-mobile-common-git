package com.yomstar.app.config
{
	public class YomstarAPIGates
	{
		public static const AUTHENTICATOR_SEREVER_URL:String = "https://api.dev.yomstar.com/authenticate";
		public static const PLANS_DETAILS_URL:String = "https://api.dev.yomstar.com/plan/all";
		public static const PARTICIPANT_DETAILS_URL:String = "https://api.dev.yomstar.com/participant/";
		//public static const REWARD_DETAILS_FROM_CODE_URL:String = "https://api.dev.yomstar.com/reward/getRewardFromCode/";
		public static const REWARD_DETAILS_FROM_CODE_URL:String = "https://api.dev.yomstar.com/reward/reward_from_code/";
		public static const REWARD_SCHEME_FROM_CODE_URL:String = "https://api.dev.yomstar.com/reward/from_code/";
		public static const ACCOUNT_STATISTICS_URL:String = "https://api.dev.yomstar.com/account/statistic/";
		public static const ACCOUNT_DETAILS_URL:String = "https://api.dev.yomstar.com/account/mine/";
		public static const SURVEY_RESPONSE_URL:String = "https://api.dev.yomstar.com/report/filteredsamples/";
		//public static const SURVEY_RESPONSE_URL:String = "http://cn-grails-dev:9001/baseline-server/report/filteredsamples/";
		public static const BASELINE_STATISTICS_URL:String = "https://api.dev.yomstar.com/report/baseline_statistics/";
		public static const USER_INFO_SEREVER_URL:String = "https://api.dev.yomstar.com/user/me";
		public static const USER_OWNED_SURVEY_URL:String = "https://api.dev.yomstar.com/baseline/owned_by_me/";
		public static const SURVEY_BY_BASELINE_ID_URL:String = "https://api.dev.yomstar.com/baseline/";
		public static const SAMPLE_DETAILS_URL:String = "https://api.dev.yomstar.com/sample/";
	}
	/*CURL Command
	curl --insecure -X POST --data '{"sort":[{"sampleDateCreated":"desc"}], "baseline":[{"name":{"like":"Mandatory + Photo"}}]}'	 -H 'Content-Type:application/json'  -H  "key:FmAdury2Q3OxjvW2Smx3Wg" "https://api.dev.yomstar.com/report/filteredsamples/2/0"
	*/
}