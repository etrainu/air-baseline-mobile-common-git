package com.yomstar.app.dataObjects
{

	public class QuestionData
	{
		public var id:String;
		public var type:String;
		public var questionText:String;
		public var order:int;
		public var isRequired:Boolean;
		public var allowMediaCapture:Boolean;
		public var metrics:Array;
		public function QuestionData()
		{
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", type: "+type);
			str = str.concat(", questionText: "+questionText);
			str = str.concat(", order: "+order);
			str = str.concat(", order: "+order);
			str = str.concat(", metrics: "+metrics);
			return str
		}
	}
}