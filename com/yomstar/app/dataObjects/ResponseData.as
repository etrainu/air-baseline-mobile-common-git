package com.yomstar.app.dataObjects
{
	public class ResponseData
	{
		public var type:String;
		public var questionId:String;
		public var questionText:String;
		public var score:int;
		public function ResponseData()
		{
		}
		public function toString():String{
			var str:String = "type: "+type;
			str = str.concat(", questionId: "+questionId);
			str = str.concat(", questionText: "+questionText);
			str = str.concat(", score: "+score);
			return str
		}
	}
}