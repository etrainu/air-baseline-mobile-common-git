package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class SampleDetail
	{
		public var id:String;
		public var theme:String;
		public var participant:String;
		public var rewardCode:String;
		public var rewardScheme:String;
		public var baseline:String;
		public var versionNumber:String;
		public var latitude:String;
		public var longitude:String;
		public var isGeolocated:Boolean;
		public var ipAddress:String;
		public var dateCreated:String;
		public var averagePercentageScore:int;
		public var answers:Array;
		public function SampleDetail(value:*="")
		{
			if(value.id && value.baseline && value.dateCreated){
				this.id = String(value.id);
				this.theme = String(value.theme);
				this.participant = String(value.participant);
				this.rewardCode = String(value.rewardCode);
				this.rewardScheme = String(value.rewardScheme);
				this.baseline = String(value.baseline);
				this.versionNumber = String(value.versionNumber);
				this.latitude = String(value.latitude);
				this.longitude = String(value.longitude);
				this.isGeolocated = BooleanParser.parse(value.isGeolocated);
				this.ipAddress = String(value.ipAddress);
				this.dateCreated = String(value.dateCreated);
				this.averagePercentageScore = Number(value.averagePercentageScore);
				this.answers = value.answers as Array;
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", theme: "+theme);
			str = str.concat(", participant: "+participant);
			str = str.concat(", rewardCode: "+rewardCode);
			str = str.concat(", rewardScheme: "+rewardScheme);
			str = str.concat(", baseline: "+baseline);
			str = str.concat(", versionNumber: "+versionNumber);
			str = str.concat(", latitude: "+latitude);
			str = str.concat(", longitude: "+longitude);
			str = str.concat(", isGeolocated: "+isGeolocated);
			str = str.concat(", ipAddress: "+ipAddress);
			str = str.concat(", dateCreated: "+dateCreated);
			str = str.concat(", averagePercentageScore: "+averagePercentageScore);
			str = str.concat(", answers: "+answers);
			return str
		}
	}
}