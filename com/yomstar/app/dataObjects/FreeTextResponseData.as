package com.yomstar.app.dataObjects
{
	public class FreeTextResponseData extends ResponseData
	{
		public var response:String;
		public function FreeTextResponseData(value:*="")
		{
			if(value.type && value.question){
				this.type = String(value.type);
				this.score = Number(value.score);
				this.response = String(value.response);
				this.questionId = String(value.question.id);
				this.questionText = String(value.question.text);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", response: "+response);
			return str
		}
	}
}