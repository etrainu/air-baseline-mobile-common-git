package com.yomstar.app.dataObjects
{
	public class MediaResponseData extends ResponseData
	{
		public var mediaCaptureURI:String;
		public var mediaCaptureHost:String;
		public function MediaResponseData(value:*="")
		{
			if(value.type && value.question){
				this.type = String(value.type);
				this.score = Number(value.score);
				this.questionId = String(value.question.id);
				this.questionText = String(value.question.text);
				this.mediaCaptureURI =String(value.mediaCaptureURI);
				this.mediaCaptureHost = String(value.mediaCaptureHost);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", mediaCaptureHost: "+mediaCaptureHost);
			str = str.concat(", mediaCaptureURI: "+mediaCaptureURI);
			return str
		}
	}
}