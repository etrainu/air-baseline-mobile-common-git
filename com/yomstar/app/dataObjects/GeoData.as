package com.yomstar.app.dataObjects
{
	public class GeoData
	{
		public static var TILE_FORMAT_PNG:String = "png";
		public static var TILE_FORMAT_JPEG:String = "jpeg";
		public static var TILE_FORMAT_GIF:String = "gif";
		//API Refrence http://msdn.microsoft.com/en-us/library/ff701724.aspx
		public var latitude:String;
		public var longitude:String;
		public var zoom:uint;
		public var tileFromat:String;
		public var tileWidth:uint;
		public var tileHeight:uint;
		public function GeoData()
		{
			
		}
		public function init(latitude:String, longitude:String, tileFromat:String, zoom:uint = 5, tileWidth:uint = 400, tileHeight:uint=400):void{
			this.latitude = latitude;
			this.longitude = longitude;
			this.zoom = zoom;
			this.tileFromat = tileFromat;
			this.tileWidth = tileWidth;
			this.tileHeight = tileHeight;
		}
		public function toString():String{
			var str:String = "latitude: "+latitude;
			str = str.concat(", longitude: "+longitude);
			str = str.concat(", zoom: "+zoom);
			str = str.concat(", tileFromat: "+tileFromat);
			str = str.concat(", tileWidth: "+tileWidth);
			str = str.concat(", tileHeight: "+tileHeight);
			return str
		}
	}
}