package com.yomstar.app.dataObjects
{
	public class BaselineData
	{
		public var baselines:Vector.<Baseline>;
		public function BaselineData(value:*="")
		{
			if(value.baselines){
				baselines = new Vector.<Baseline>();
				var baselinesArr:Array = value.baselines as Array;
				var len:int = baselinesArr.length;
				var baseline:Baseline;
				for(var i:int=0; i<len; i++){
					baseline = new Baseline(baselinesArr[i]);
					baselines.push(baseline);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "baselines: ["+baselines+"]";
			return str
		}
	}
}