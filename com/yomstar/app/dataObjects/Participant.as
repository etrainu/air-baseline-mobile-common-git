package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class Participant
	{
		public var id:String;
		public var firstName:String;
		public var lastName:String;
		public var email:String;
		public var phone:String;
		public var directMarketing:Boolean;
		public var dateCreated:String;
		public var account:Array;
		public function Participant(value:*="")
		{
			if(value.id && value.email){
				this.id = String(value.id);
				this.firstName = String(value.firstName);
				this.lastName = String(value.lastName);
				this.email = String(value.email);
				this.phone = String(value.phone);
				this.directMarketing = BooleanParser.parse(value.directMarketing);
				this.dateCreated = String(value.dateCreated);
				this.account = value.account as Array;
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", firstName: "+firstName);
			str = str.concat(", lastName: "+lastName);
			str = str.concat(", email: "+email);
			str = str.concat(", phone: "+phone);
			str = str.concat(", directMarketing: "+directMarketing);
			str = str.concat(", dateCreated: "+dateCreated);
			str = str.concat(", account: "+account);
			return str
		}
	}
}