package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class Baseline
	{
		public var id:String;
		public var name:String;
		public var dateCreated:String;
		public var creatorId:String;
		public var creatorName:String;
		public var versionNumber:int;
		public var isPublic:Boolean;
		public var isReleased:Boolean;
		public var allowAnonymous:Boolean;
		public var accountId:String;
		public var accountName:String;
		public var hasRecommendations:Boolean;
		public var publicResults:Boolean;
		public var shareURI:String;
		public var shortURL:String;
		public var sampleCount:int;
		public var firstSample:String;
		public var lastSample:String;
		public function Baseline(value:*="")
		{
			if(value.id && value.dateCreated){
				this.id = String(value.id);
				this.name = String(value.name);
				this.dateCreated = String(value.dateCreated);
				if(value.createdBy){
					this.creatorId = String(value.createdBy.id);
					this.creatorName = String(value.createdBy.name);
				}
				this.versionNumber = Number(value.versionNumber);
				this.isPublic = BooleanParser.parse(value.isPublic);
				this.isReleased = BooleanParser.parse(value.isReleased);
				this.allowAnonymous = BooleanParser.parse(value.allowAnonymous);
				if(value.account){
					this.accountId = String(value.account.id);
					this.accountName = String(value.account.name);
				}
				this.hasRecommendations = BooleanParser.parse(value.hasRecommendations);
				this.publicResults = BooleanParser.parse(value.publicResults);
				this.shareURI = String(value.shareURI);
				this.shortURL = String(value.shortURL);
				this.sampleCount = Number(value.sampleCount);
				this.firstSample = String(value.firstSample);
				this.lastSample = String(value.lastSample);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+this.id;
			str = str.concat(", name: "+this.name);
			str = str.concat(", dateCreated: "+this.dateCreated);
			str = str.concat(", creatorId: "+this.creatorId);
			str = str.concat(", creatorName: "+this.creatorName);
			str = str.concat(", versionNumber: "+this.versionNumber);
			str = str.concat(", isPublic: "+this.isPublic);
			str = str.concat(", isReleased: "+this.isReleased);
			str = str.concat(", allowAnonymous: "+this.allowAnonymous);
			str = str.concat(", accounztId: "+this.accountId);
			str = str.concat(", accountName: "+this.accountName);
			str = str.concat(", hasRecommendations: "+this.hasRecommendations);
			str = str.concat(", publicResults: "+this.publicResults);
			str = str.concat(", shareURI: "+this.shareURI);
			str = str.concat(", shortURL: "+this.shortURL);
			str = str.concat(", sampleCount: "+this.sampleCount);
			str = str.concat(", firstSample: "+this.firstSample);
			str = str.concat(", lastSample: "+this.lastSample);
			return str
		}
	}
}