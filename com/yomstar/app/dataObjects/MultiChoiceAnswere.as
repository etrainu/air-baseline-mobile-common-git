package com.yomstar.app.dataObjects
{
	public class MultiChoiceAnswere
	{
		public var id:String;
		public var desc:String;
		public function MultiChoiceAnswere(value:*="")
		{
			if(value.id && value.desc){
				this.id = String(value.id);
				this.desc = String(value.desc);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", desc: "+desc);
			return str
		}
	}
}