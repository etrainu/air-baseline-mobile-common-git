package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class MediaQuestionData extends QuestionData
	{
		public var mediaSize:String;
		public var options:Vector.<MultiChoiceOption>;
		public function MediaQuestionData(value:*="")
		{
			if(value.type && value.options){
				this.type = String(value.type);
				this.id = String(value.id);
				this.questionText = String(value.questionText);
				this.order = Number(value.order);
				this.isRequired = BooleanParser.parse(value.isRequired);
				this.allowMediaCapture = BooleanParser.parse(value.allowMediaCapture);
				this.mediaSize = String(value.mediaSize);
				options = new Vector.<MultiChoiceOption>();
				var optionsArr:Array = value.options as Array;
				var len:int = optionsArr.length;
				var choiceOption:MultiChoiceOption;
				for(var i:int=0; i<len; i++){
					choiceOption = new MultiChoiceOption(optionsArr[i]);
					options.push(choiceOption);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", allowMediaCapture: "+allowMediaCapture);
			str = str.concat(", mediaSize: "+mediaSize);
			str = str.concat(", options: ["+options+"]");
			return str
		}
	}
}