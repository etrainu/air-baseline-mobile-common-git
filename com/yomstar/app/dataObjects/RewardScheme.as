package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class RewardScheme
	{
		public var id:String;
		public var name:String;
		public var description:String;
		public var termsAndConditions:String;
		public var termsAndConditionsURL:String;
		public var redemptionCount:String;
		public var rewardCount:String;
		public var offerFrom:String;
		public var offerUntil:String;
		public var redeemFrom:String;
		public var redeemUntil:String;
		public var dateCreated:String;
		public var lastUpdated:String;
		public var lastRewardGenerated:String;
		public var baseline:String;
		public var baselineName:String;
		public var createdBy:String;
		public var status:String;
		public function RewardScheme(value:*="")
		{
			if(value.id && value.name && value.offerFrom){
				this.id = String(value.id);
				this.name = String(value.name);
				this.description = String(value.description);
				this.termsAndConditions = String(value.termsAndConditions);
				this.termsAndConditionsURL = String(value.termsAndConditionsURL);
				this.redemptionCount = String(value.redemptionCount);
				this.rewardCount = String(value.rewardCount);
				this.offerFrom = String(value.offerFrom);
				this.offerUntil = String(value.offerUntil);
				this.redeemFrom = String(value.redeemFrom);
				this.redeemUntil = String(value.redeemUntil);
				this.dateCreated = String(value.dateCreated);
				this.lastUpdated = String(value.lastUpdated);
				this.lastRewardGenerated = String(value.lastRewardGenerated);
				this.baseline = String(value.baseline);
				this.baselineName = String(value.baselineName);
				this.createdBy = String(value.createdBy);
				this.status = String(value.status);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", name: "+name);
			str = str.concat(", description: "+description);
			str = str.concat(", termsAndConditions: "+termsAndConditions);
			str = str.concat(", termsAndConditionsURL: "+termsAndConditionsURL);
			str = str.concat(", redemptionCount: "+redemptionCount);
			str = str.concat(", rewardCount: "+rewardCount);
			str = str.concat(", offerFrom: "+offerFrom);
			str = str.concat(", offerUntil: "+offerUntil);
			str = str.concat(", redeemFrom: "+redeemFrom);
			str = str.concat(", redeemUntil: "+redeemUntil);
			str = str.concat(", dateCreated: "+dateCreated);
			str = str.concat(", lastUpdated: "+lastUpdated);
			str = str.concat(", lastRewardGenerated: "+lastRewardGenerated);
			str = str.concat(", baseline: "+baseline);
			str = str.concat(", baselineName: "+baselineName);
			str = str.concat(", createdBy: "+createdBy);
			str = str.concat(", status: "+status);
			return str
		}
	}
}