package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class ScaledQuestionData extends QuestionData
	{
		public var options:Vector.<ScaledOption>;
		public function ScaledQuestionData(value:*="")
		{
			if(value.type && value.options){
				this.type = String(value.type);
				this.id = String(value.id);
				this.questionText = String(value.questionText);
				this.order = Number(value.order);
				this.isRequired = BooleanParser.parse(value.isRequired);
				this.allowMediaCapture = BooleanParser.parse(value.allowMediaCapture);
				this.metrics = value.metrics as Array;
				
				options = new Vector.<ScaledOption>();
				var optionsArr:Array = value.options as Array;
				var len:int = optionsArr.length;
				var choiceOption:ScaledOption;
				for(var i:int=0; i<len; i++){
					choiceOption = new ScaledOption(optionsArr[i]);
					options.push(choiceOption);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", options: ["+options+"]");
			return str
		}
	}
}