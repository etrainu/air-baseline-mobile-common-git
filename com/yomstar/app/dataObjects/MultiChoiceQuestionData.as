package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class MultiChoiceQuestionData extends QuestionData
	{
		public var isMulti:Boolean;
		public var allowOther:Boolean;
		public var scoreCalculationMethod:String;
		public var options:Vector.<MultiChoiceOption>;
		public function MultiChoiceQuestionData(value:*="")
		{
			if(value.type && value.options){
				this.type = String(value.type);
				this.id = String(value.id);
				this.questionText = String(value.questionText);
				this.order = Number(value.order);
				this.isMulti = BooleanParser.parse(value.isMulti);
				this.isRequired = BooleanParser.parse(value.isRequired);
				this.allowOther = BooleanParser.parse(value.allowOther);
				this.allowMediaCapture = BooleanParser.parse(value.allowMediaCapture);
				this.scoreCalculationMethod = String(value.scoreCalculationMethod);
				this.metrics = value.metrics as Array;
				
				options = new Vector.<MultiChoiceOption>();
				var optionsArr:Array = value.options as Array;
				var len:int = optionsArr.length;
				var choiceOption:MultiChoiceOption;
				for(var i:int=0; i<len; i++){
					choiceOption = new MultiChoiceOption(optionsArr[i]);
					options.push(choiceOption);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", isMulti: "+isMulti);
			str = str.concat(", allowOther: "+allowOther);
			str = str.concat(", scoreCalculationMethod: "+scoreCalculationMethod);
			str = str.concat(", options: ["+options+"]");
			return str
		}
	}
}