package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class FreeTextQuestionData extends QuestionData
	{
		public var isAutoMark:Boolean;
		
		public function FreeTextQuestionData(value:*="")
		{
			if(value.type && value.options){
				this.type = String(value.type);
				this.id = String(value.id);
				this.questionText = String(value.questionText);
				this.order = Number(value.order);
				this.isAutoMark = BooleanParser.parse(value.isAutoMark);
				this.isRequired = BooleanParser.parse(value.isRequired);
				this.allowMediaCapture = BooleanParser.parse(value.allowMediaCapture);
				this.metrics = value.metrics as Array;
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", isAutoMark: "+isAutoMark);
			return str
		}
	}
}