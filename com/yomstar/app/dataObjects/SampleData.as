package com.yomstar.app.dataObjects
{
	public class SampleData
	{
		public var samples:Vector.<Sample>;
		private var _length:int = -1;
		public function SampleData(value:*="")
		{
			if(value != ""){
				samples = new Vector.<Sample>();
				var samplesArr:Array = value as Array;
				var len:int = samplesArr.length;
				var sample:Sample;
				for(var i:int=0; i<len; i++){
					sample = new Sample(samplesArr[i]);
					samples.push(sample);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}

		public function get length():int
		{
			if(samples){
				_length = samples.length;
			}
			return _length;
		}

		public function toString():String{
			var str:String = "samples: ["+samples+"]";
			return str
		}
	}
}