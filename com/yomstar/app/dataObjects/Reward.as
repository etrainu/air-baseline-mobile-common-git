package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class Reward
	{
		public var code:String;
		public var activatedDate:String;
		public var dateCreated:String;
		public var redeemedDate:String;
		public var rewardScheme:String;
		public var sample:String;
		public var requiresPin:String;
		public var activatedBy:Participant;
		public var redeemedBy:Participant;
		public function Reward(value:*="")
		{
			if(value.code && value.activatedDate && value.dateCreated){
				this.code = String(value.code);
				this.activatedDate = String(value.activatedDate);
				this.dateCreated = String(value.dateCreated);
				this.redeemedDate = String(value.redeemedDate);
				this.rewardScheme = String(value.rewardScheme);
				this.sample = String(value.sample);
				this.requiresPin = String(value.requiresPin);
				if(value.activatedBy){
					this.activatedBy = new Participant(value.activatedBy);
				}
				if(value.redeemedBy){
					this.redeemedBy = new Participant(value.redeemedBy);
				}
				
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "code: "+code;
			str = str.concat(", activatedDate: "+activatedDate);
			str = str.concat(", dateCreated: "+dateCreated);
			str = str.concat(", redeemedDate: "+redeemedDate);
			str = str.concat(", rewardScheme: "+rewardScheme);
			str = str.concat(", sample: "+sample);
			str = str.concat(", requiresPin: "+requiresPin);
			str = str.concat(", activatedBy: "+activatedBy);
			str = str.concat(", redeemedBy: "+redeemedBy);
			return str
		}
	}
}