package com.yomstar.app.dataObjects
{
	public class ResponseCaptureData
	{
		public var respondantFullName:String;
		public var respondantEmail:String;
		public var respondantPhone:String;
		public var captureDate:String;
		public var getoData:GeoData;
		public function ResponseCaptureData(respondantFullName:String = "", respondantEmail:String = "", respondantPhone:String = "", captureDate:String = "", getoData:GeoData = null)
		{
			this.respondantFullName = respondantFullName;
			this.respondantEmail = respondantEmail;
			this.respondantPhone = respondantPhone;
			this.captureDate = captureDate;
			this.getoData = getoData;
		}
		public function toString():String{
			var str:String = "respondantFullName: "+respondantFullName;
			str = str.concat(", respondantEmail: "+respondantEmail);
			str = str.concat(", respondantPhone: "+respondantPhone);
			str = str.concat(", captureDate: "+captureDate);
			str = str.concat(", getoData: "+getoData);
			return str
		}
	}
}