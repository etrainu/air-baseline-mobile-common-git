package com.yomstar.app.dataObjects
{
	public class MultiChoiceResponseData extends ResponseData
	{
		public var responses:Vector.<MultiChoiceAnswere>;
		public function MultiChoiceResponseData(value:*="")
		{
			if(value.type && value.question){
				this.type = String(value.type);
				this.score = Number(value.score);
				this.questionId = String(value.question.id);
				this.questionText = String(value.question.text);
				
				responses = new Vector.<MultiChoiceAnswere>();
				var responsesArr:Array = value.responses as Array;
				var len:int = responsesArr.length;
				var answere:MultiChoiceAnswere;
				for(var i:int=0; i<len; i++){
					answere = new MultiChoiceAnswere(responsesArr[i]);
					responses.push(answere);
				}
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", response: ["+responses+"]");
			return str
		}
	}
}