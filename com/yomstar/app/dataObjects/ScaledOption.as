package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class ScaledOption
	{
		public var type:String;
		public var id:String;
		public var description:String;
		public var value:int;
		public var isDefault:Boolean;
		public function ScaledOption(value:*="")
		{
			if(value.type && value.id){
				this.type = String(value.type);
				this.id = String(value.id);
				this.description = String(value.description);
				if(value.value && value.value != undefined && value.value != "undefined"){
					this.value = Number(value.value);
				}else{
					this.value = -1;
				}
				this.isDefault = BooleanParser.parse(value.isDefault);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "id: "+id;
			str = str.concat(", type: "+type);
			str = str.concat(", description: "+description);
			str = str.concat(", value: "+value);
			str = str.concat(", isDefault: "+isDefault);
			return str
		}
	}
}