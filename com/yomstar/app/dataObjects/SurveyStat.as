package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;
	import com.yomstar.app.utils.ObjectDigger;

	public class SurveyStat
	{
		public var sampleWithActivatedRewardCount:int;
		public var sampleWithRewardCount:int;
		public var sampleWithRedeemedRewardCount:int;
		public var sampleCount:int;
		public var viewCount:int;
		public var sampleWithParticipantCount:int;
		public function SurveyStat(value:*="")
		{
			if(value.sampleCount>=0){
				this.sampleWithActivatedRewardCount = Number(value.sampleWithActivatedRewardCount);
				this.sampleWithRewardCount = Number(value.sampleWithRewardCount);
				this.sampleWithRedeemedRewardCount = Number(value.sampleWithRedeemedRewardCount);
				this.sampleCount = Number(value.sampleCount);
				this.viewCount = Number(value.viewCount);
				this.sampleWithParticipantCount = Number(value.sampleWithParticipantCount);
			}else{
				throw new Error("Object passed to the SurveyStat constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "sampleWithActivatedRewardCount: "+sampleWithActivatedRewardCount;
			str = str.concat(", sampleWithRewardCount: "+sampleWithRewardCount);
			str = str.concat(", sampleWithRedeemedRewardCount: "+sampleWithRedeemedRewardCount);
			str = str.concat(", sampleCount: "+sampleCount);
			str = str.concat(", viewCount: "+viewCount);
			str = str.concat(", sampleWithParticipantCount: "+sampleWithParticipantCount);
			return str
		}
	}
}