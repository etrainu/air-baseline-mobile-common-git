package com.yomstar.app.dataObjects
{
	public class ScaledResponseData extends ResponseData
	{
		public var response:ScaledAnswere;
		public function ScaledResponseData(value:*="")
		{
			if(value.type && value.question){
				this.type = String(value.type);
				this.score = Number(value.score);
				this.questionId = String(value.question.id);
				this.questionText = String(value.question.text);
				this.response = new ScaledAnswere(value.response);
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		override public function toString():String{
			var str:String = super.toString();
			str = str.concat(", response: "+response);
			return str
		}
	}
}