package com.yomstar.app.dataObjects
{
	import com.yomstar.app.utils.BooleanParser;

	public class Sample
	{
		public var externalId:String;
		public var baselineName:String;
		public var version:String;
		public var latitude:String;
		public var longitude:String;
		public var isGeolocated:Boolean;
		public var baselineExternalId:String;
		public var dateCaptured:String;
		public var averagePercentageScore:int;
		public var questions:Array;
		public function Sample(value:*="")
		{
			if(value.externalId && value.baselineName && value.dateCaptured){
				this.externalId = String(value.externalId);
				this.baselineName = String(value.baselineName);
				this.version = String(value.version);
				this.latitude = String(value.latitude);
				this.longitude = String(value.longitude);
				this.isGeolocated = BooleanParser.parse(value.isGeolocated);
				this.baselineExternalId = String(value.baselineExternalId);
				this.dateCaptured = String(value.dateCaptured);
				this.averagePercentageScore = Number(value.averagePercentageScore);
				this.questions = value.questions as Array;
			}else{
				throw new Error("Object passed to the constructor is not of a valid format");
			}
		}
		public function toString():String{
			var str:String = "externalId: "+externalId;
			str = str.concat(", baselineName: "+baselineName);
			str = str.concat(", baselineExternalId: "+baselineExternalId);
			str = str.concat(", version: "+version);
			str = str.concat(", latitude: "+latitude);
			str = str.concat(", longitude: "+longitude);
			str = str.concat(", isGeolocated: "+isGeolocated);
			str = str.concat(", dateCaptured: "+dateCaptured);
			str = str.concat(", averagePercentageScore: "+averagePercentageScore);
			str = str.concat(", questions: "+questions);
			return str
		}
	}
}