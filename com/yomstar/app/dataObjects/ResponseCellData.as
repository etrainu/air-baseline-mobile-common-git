package com.yomstar.app.dataObjects
{
	public class ResponseCellData
	{
		public var capture:ResponseCaptureData;
		public var question:QuestionData;
		public var ans:ResponseData;
		public function ResponseCellData(capture:ResponseCaptureData = null, question:QuestionData = null, ans:ResponseData=null)
		{
			this.capture = capture;
			this.question = question;
			this.ans = ans;
		}
		public function toString():String{
			var str:String = "capture: "+capture;
			str = str.concat(", question: "+question);
			str = str.concat(", ans: "+ans);
			return str
		}
	}
}