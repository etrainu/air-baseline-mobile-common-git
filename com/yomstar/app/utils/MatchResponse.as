package com.yomstar.app.utils
{
	import com.yomstar.app.dataObjects.ResponseData;

	public class MatchResponse
	{
		public static function getAnsIndexById(questionId:String, ansList:Vector.<ResponseData>):int
		{
			var ansIndex:int = -1;
			var len:int = ansList.length;
			for(var i:int = 0; i<len; i++){
				if(ansList[i].questionId == questionId){
					ansIndex = i;
					break
				}
			}
			return ansIndex
		}
	}
}