package com.yomstar.app.utils
{
	public class Mood
	{
		private static const MOOD_COLORS:Vector.<Number> = new <Number>
			[
				0xcbcbcb, 0xcb2026, 0xec6d0b, 0xf2ed00, 0x8fdf1e, 0x14a700
			];
		public static function getMoodIdByValue(value:int):int{
			if(value >= 0){
				return Math.floor(value/20);
			}else{
				return -1;
			}
		}
		public static function getMoodColorByValue(value:int):Number{
			if(value){
				return MOOD_COLORS[getMoodIdByValue(value)];
			}else{
				return 0x000000;
			}
			
		}
	}
}