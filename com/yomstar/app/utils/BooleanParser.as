package com.yomstar.app.utils
{
	public class BooleanParser
	{
		public static function parse(value:*):Boolean{
			return (String(value).toLowerCase() == "true");
		}
	}
}