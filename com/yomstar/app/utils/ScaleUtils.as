package com.yomstar.app.utils
{
	import starling.display.DisplayObject;

	public class ScaleUtils
	{
		public static function getConstrainedScaleRatio(targetWidth:uint, targetHeight:uint, scalableObject:DisplayObject, allowEnlargement:Boolean= false):Number{
			var ratio:Number = Math.min((targetWidth/(scalableObject.width/scalableObject.scaleX)), (targetHeight/(scalableObject.height/scalableObject.scaleY)));
			if(!allowEnlargement){
				return Math.min(ratio, 1);
			}else{
				return ratio
			}
		}
	}
}