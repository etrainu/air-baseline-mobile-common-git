package com.yomstar.app.utils
{
	public class DateFormat
	{
		public static function stringify(n:Number):String{
			if(n>9)
				return String(n)
			else
				return "0"+String(n)
		}
	}
}