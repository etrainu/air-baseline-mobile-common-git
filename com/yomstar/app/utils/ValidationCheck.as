package com.yomstar.app.utils
{
	public class ValidationCheck
	{
		private static const emailCheckPattern:RegExp = /(\w|[_.\-])+@((\w|-)+\.)+\w{2,4}+/;
		public function ValidationCheck()
		{
		}
		public static function validateEmail(str:String):Boolean {
			var result:Object = emailCheckPattern.exec(str);
			if(result == null) {
				return false;
			}
			return true;
		}

	}
}