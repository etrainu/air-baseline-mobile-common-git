package com.yomstar.app.utils
{
	public class ObjectDigger
	{
		public static function digg(obj:*, maxChars:int = -1):String  {
			var p:*;
			var res:String = '';
			var val:String;
			var prop:String;
			for (p in obj) {
				prop = String(p);
				if (prop && prop!=='' && prop!==' ') {
					val = String(obj[p]);
					if (val.length>maxChars && maxChars>0) val = val.substr(0,maxChars)+'...';
					res += prop+':'+val+', ';
				}
			}
			res = res.substr(0, res.length-2);
			return res;
		}
	}
}