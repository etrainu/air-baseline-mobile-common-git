package com.yomstar.app.utils
{
	import com.yomstar.app.dataObjects.MultiChoiceAnswere;

	public class MatchChoices
	{
		public static function getChoiceIndexById(choiceId:String, ansList:Vector.<MultiChoiceAnswere>):int
		{
			var ansIndex:int = -1;
			var len:int = ansList.length;
			for(var i:int = 0; i<len; i++){
				if(ansList[i].id == choiceId){
					ansIndex = i;
					break
				}
			}
			return ansIndex
		}
	}
}