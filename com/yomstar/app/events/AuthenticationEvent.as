package com.yomstar.app.events
{
	import flash.events.Event;
	
	public class AuthenticationEvent extends Event
	{
		public static const LOGIN_SUCCESS:String = "loginSuccess";
		public static const LOGIN_FAILURE:String = "loginFailure";
		public var token:String = "";
		public var message:String = "";
		public var success:Boolean = false;
		
		public function AuthenticationEvent(type:String, token:String, success:Boolean, message:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.token = token;
			this.success = success;
			this.message = message;
			super(type, bubbles, cancelable);
		}
		override public function clone():Event{
			var eventObj:AuthenticationEvent = new AuthenticationEvent(type, this.token, this.success, message, bubbles, cancelable);
			return eventObj;
		}
		public override function toString():String
		{
			return formatToString("AuthenticationEvent", "token", "success", "message", "type", "bubbles", "cancelable");
		}
	}
}