package com.yomstar.app.events
{
	import flash.events.Event;
	
	import starling.textures.Texture;
	
	public class ImageLoaderEvent extends Event
	{
		public static const IMAGE_RECIEVED:String = "imageReceived";
		public var imageTexture:Texture;
		
		public function ImageLoaderEvent(type:String, imageTexture:Texture, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.imageTexture = imageTexture;
			super(type, bubbles, cancelable);
		}
		override public function clone():Event{
			var eventObj:ImageLoaderEvent = new ImageLoaderEvent(type, this.imageTexture, bubbles, cancelable);
			return eventObj;
		}
		public override function toString():String
		{
			return formatToString("AuthenticationEvent", "imageTexture", "type", "bubbles", "cancelable");
		}
	}
}